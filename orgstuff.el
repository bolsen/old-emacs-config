;; load up todo list on startup
(find-file "~/org/todo.org")
(find-file "~/org/todo2.org")

(setq org-agenda-files (list "~/org/todo.org"
							 "~/org/todo2.org"
                             "~/org/home.org"))

;; Make windmove work in org-mode
(add-hook 'org-shiftup-final-hook 'windmove-up)
(add-hook 'org-shiftleft-final-hook 'windmove-left)
(add-hook 'org-shiftdown-final-hook 'windmove-down)
(add-hook 'org-shiftright-final-hook 'windmove-right)



