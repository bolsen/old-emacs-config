;; autotest.el - by Ryan Davis - ryan-ruby@zenspider.com
;;
;; Sets up an autotest buffer and provides conveniencte methods.
;;
;; History:
;; 1.0 beta 1 - 2007-03-06 - initial release

(require 'shell)

(defun autotest-using-comint (name)
  "Fire up an instance of autotest in its own buffer with shell bindings and compile-mode highlighting and linking."
  (interactive)
  ;; TODO: (if (not (comint-check-proc "*autotest*"))

  (let ((buffer (shell (concat "*" name "*"))))

    (define-key shell-mode-map "\C-c\C-a" 'autotest-switch)

    (set (make-local-variable 'comint-output-filter-functions)
         '(comint-truncate-buffer comint-postoutput-scroll-to-bottom))
    (set (make-local-variable 'comint-buffer-maximum-size) 5000)
    (set (make-local-variable 'comint-scroll-show-maximum-output) t)
    (set (make-local-variable 'comint-scroll-to-bottom-on-output) t)

    (set (make-local-variable 'compilation-error-regexp-alist)
         '(
           ("^ +\\([^:]+\\):\\([0-9]+\\)" 1 2)
           ("\\[\\(.*\\):\\([0-9]+\\)\\]:$" 1 2)
           ; ("^ *\\[?\\([^:\\n\\r]+\\):\\([0-9]+\\):in" 1 2)
           ))
    (compilation-shell-minor-mode)
    (comint-send-string buffer  (concat name "\n"))))

(defun autotest-switch ()
  "Switch back and forth between autotest and the previous buffer"
  (interactive)
  (if (equal "*autotest*" (buffer-name))
      (switch-to-buffer nil)
    (switch-to-buffer "*autotest*")))


(defun autotest ()
  (interactive)
  (autotest-using-comint "autotest")
  )

(defun autospec ()
  (interactive)
  (autotest-using-comint "autospec")
  )

(provide 'autotest)
